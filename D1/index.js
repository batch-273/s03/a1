// alert("Hello!");

//In JS, classes can be created yung the "class" keyword and {}
/*

//
	
Naming convention for classes: Begin with Uppercase characters

	Syntax:
		class NameOfClass {
			
		}

*/

/*
	Mini-Activity:
		Created a new class called Person

		This person class should be able to isntatiate a new object with the ff fields

		name,
		age (should be a number and must be greater than or equal to 18, otherwise set the property to undefined),
		nationality,
		address

		Instantiate 2 new objects from the Person class. person1 and person2

		Log both object in the console.

*/

class Person {
  constructor(name, age, nationality, address) {
    (this.name = name),
      (this.age = typeof age === "number" && age >= 18 ? age : undefined),
      (this.nationality = nationality),
      (this.address = address);
  }
}

let person1 = new Person("John", "s", "American", "Washington");

console.log(person1);

let person2 = new Person("Jane", 19, "British", "New York");

console.log(person2);

//Acivity S03-1

/*
 [Part 1]
 1. Class
 2. Proper Case
 3. new
 4. instantiation
 5. constructor
*/

//[Part 2]

class Student {
  constructor(name, email, grades) {
    //this.key = value/parameter
    this.name = name;
    this.email = email;
    this.gradeAverade = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;
    this.grades =
      grades.every((grade) => grade >= 0 && grade <= 100) && grades.length === 4
        ? grades
        : undefined;
  }
  login() {
    console.log(`${this.email} has logged in.`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out.`);
    return this;
  }
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    return this;
  }
  computeAve() {
    let sum = this.grades.reduce((x, y) => x + y, 0);
    let ave = sum / this.grades.length;
    this.gradeAverade = ave;
    console.log(ave);
    return this;
  }
  willPass() {
    this.passed = this.computeAve() >= 85 ? true : false;
    return this;
  }
  willPassWithHonors() {
    this.passedWithHonors = this.willPass()
      ? this.computeAve() >= 90
        ? true
        : false
      : undefined;
    return this;
  }
}
console.log("[Part 1]");
// let studentTest = new Student("John", "john@mail.com", [89, 84, 78, 88]);
// console.log(studentTest);
// let studentTest2 = new Student("John", "john@mail.com", [101, 84, 78, 88]);
// console.log(studentTest2);
// let studentTest3 = new Student("John", "john@mail.com", [-10, 84, 78, 88]);
// console.log(studentTest3);
// let studentTest4 = new Student("John", "john@mail.com", ["hello", 84, 78, 88]);
// console.log(studentTest4);

console.log("[Part 2]");
let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);
console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

//Acivity S03-2

/*
 1. No
 2. No
 3. Yes
 4. Setter & Getter
 5. this
*/
